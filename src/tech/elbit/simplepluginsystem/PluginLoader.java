package tech.elbit.simplepluginsystem;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import tech.elbit.simplepluginsystem.exception.PluginLoadingException;
import tech.elbit.simplepluginsystem.logger.DefaultSimplePluginSystemLogger;
import tech.elbit.simplepluginsystem.logger.SimplePluginSystemLogger;
import tech.elbit.simplepluginsystem.manifest.Manifest;
import tech.elbit.simplepluginsystem.manifest.ManifestInterpreter;

/**
 * Class used to load Plugin Class (represented by T).
 * 
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 * 
 * @param <T> Class representing your plugin
 */
public class PluginLoader<T> {

    List<ConstructorParameter> constructorParameters = new ArrayList<>();
    
    List<SimplePluginSystemLogger> loggers = new ArrayList<>();
    
    /**
     * Class constructor specifying parameters used to find constructor and create object of Class T.
     * 
     * @param constructorParameters Constructor parameters.
     * @see ConstructorParameter
     */
    public PluginLoader(ConstructorParameter... constructorParameters) {
        this.constructorParameters = Arrays.asList(constructorParameters);
    }
    
    /**
     * Class constructor.
     */
    public PluginLoader() {
        
    }
    
    /**
     * Adds logger to loggers list.
     * 
     * @param logger Logger to add.
     */
    public void addLogger(SimplePluginSystemLogger logger) {
        loggers.add(logger);
    }
    
    /**
     * Removes logger from loggers list.
     * 
     * @param logger Logger to remove.
     */
    public void removeLogger(SimplePluginSystemLogger logger) {
        loggers.remove(logger);
    }
    
    /**
     * Removes logger from loggers list via logger index.
     * 
     * @param index Logger index.
     */
    public void removeLogger(int index) {
        loggers.remove(index);
    }
    
    /**
     * Clears all loggers from loggers list.
     */
    public void clearLoggers() {
        loggers.clear();
    }
    
    /**
     * Loads Plugin object from specified class from specified .jar file.
     * 
     * @param jarFile Plugin .jar file
     * @param className Name of the main plugin class (must extend T)
     * @return Plugin object
     */
    public T loadPlugin(File jarFile, String className) {
        T plugin = null;
        try {
            Class pluginClass = getClassFromJarFile(jarFile, className);
            plugin = (T)createObjectFromClass(pluginClass, constructorParameters);
        } catch (ClassNotFoundException ex) {
            log(SimplePluginSystemLogger.WARN, "Cannot find given class inside given .jar file.", ex.getCause());
            throw new PluginLoadingException("Cannot find given class inside given .jar file", ex);
        } catch (MalformedURLException ex) {
            //should not be happening
            log(SimplePluginSystemLogger.ERROR, "There was a problem while loading a plugin.", ex.getCause());
            throw new PluginLoadingException("There was a problem while loading a plugin", ex);
        } catch (ClassCastException ex) {
            log(SimplePluginSystemLogger.WARN, "Class does not extend/implement given class/interface.", ex.getCause());
            throw new PluginLoadingException("Class does not extend/implement given class/interface", ex);
        }
        return plugin;
    }
    
    /**
     * Loads plugin and interpreter from specified .jar file.
     * Manifest will be searched for by file name.
     * 
     * @param jarFile File containing plugin class derived from manifest
     * @param interpreter Interpreter that will be used to interpret manifest
     * @param manifestName First file with that name will count as manifest
     * @return Plugin object
     */
    public T loadPlugin(File jarFile, ManifestInterpreter interpreter, String manifestName) {
        String manifest = Manifest.getManifestForFile(jarFile, manifestName);
        if (manifest == null) return null;
        interpreter.setManifest(manifest);
        return loadPlugin(jarFile, interpreter.getPluginClass());
    }
    
    /**
     * Loads plugin and interpreter from specified .jar file.
     * Manifest will be searched for by file name.
     * 
     * @param jarFile File containing plugin class derived from manifest
     * @param interpreter Interpreter that will be used to interpret manifest
     * @param manifestName First file with that name will count as manifest
     * @return List of plugins and interpreters wrapped in PluginInterpreterPair object
     */
    public PluginInterpreterPair<T> loadPluginAndInterpreter(File jarFile, ManifestInterpreter interpreter, String manifestName) {
        String manifest = Manifest.getManifestForFile(jarFile, manifestName);
        if (manifest == null) return null;
        interpreter.setManifest(manifest);
        return new PluginInterpreterPair(loadPlugin(jarFile, interpreter.getPluginClass()), interpreter);
    }
    
    /**
     * Loads plugin and interpreter from specified .jar file.
     * Manifest will be searched for by file extension.
     * 
     * @param jarFile File containing plugin class derived from manifest
     * @param interpreter Interpreter that will be used to interpret manifest
     * @param manifestExtension First file with that extension will count as manifest
     * @return Plugin object
     */
    public T loadPluginUsingManifestExtension(File jarFile, ManifestInterpreter interpreter, String manifestExtension) {
        String manifest = Manifest.getManifestForFileExtension(jarFile, manifestExtension);
        if (manifest == null) return null;
        interpreter.setManifest(manifest);
        return loadPlugin(jarFile, interpreter.getPluginClass());
    }
    
    /**
     * Loads plugin and interpreter from specified .jar file.
     * Manifest will be searched for by file extension.
     * 
     * @param jarFile File containing plugin class derived from manifest
     * @param interpreter Interpreter that will be used to interpret manifest
     * @param manifestExtension First file with that extension will count as manifest
     * @return List of plugins and interpreters wrapped in PluginInterpreterPair object
     */
    public PluginInterpreterPair<T> loadPluginAndInterpreterWithManifestExtension(File jarFile, ManifestInterpreter interpreter, String manifestExtension) {
        String manifest = Manifest.getManifestForFileExtension(jarFile, manifestExtension);
        if (manifest == null) return null;
        interpreter.setManifest(manifest);
        return new PluginInterpreterPair(loadPlugin(jarFile, interpreter.getPluginClass()), interpreter);
    }
    
    /**
     * Loads plugins from specified directory.
     * Plugin class will be searched for by class name.
     * 
     * @param directory Directory containing plugin .jar files
     * @param className Class with specified name will count as plugin class
     * @return List of plugin objects
     */
    public List<T> loadPlugins(File directory, String className) {
        List<T> plugins = new ArrayList<>();
        if (directory.isDirectory()) {
            for (File file : directory.listFiles()) {
                if (file.isDirectory()) {
                    plugins.addAll(loadPlugins(file, className));
                    continue;
                }
                try {
                    plugins.add(loadPlugin(file, className));
                } catch (PluginLoadingException ex) {
                    if (ex.getCause() instanceof ClassNotFoundException) {
                        log(SimplePluginSystemLogger.WARN, "Cannot find specified Main Class.", ex.getCause());
                    } else if (ex.getCause() instanceof ClassCastException) {
                        log(SimplePluginSystemLogger.WARN, "Class does not extend/implement given class/interface.", ex.getCause());
                    } else {
                        log(SimplePluginSystemLogger.ERROR, "Exception encountered while loading plugins.", ex.getCause());
                        throw ex;
                    }
                }
            }
        }
        return plugins;
    }
    
    /**
     * Loads plugins from specified directory.
     * Manifest will be searched for by file name.
     * 
     * @param directory Directory containing plugin .jar files
     * @param interpreter Interpreter that will be used to interpret manifest
     * @param manifestName First file with that name will count as manifest
     * @return List of plugin objects
     */
    public List<T> loadPlugins(File directory, ManifestInterpreter interpreter, String manifestName) {
        List<T> plugins = new ArrayList<>();
        if (directory.isDirectory()) {
            for (File file : directory.listFiles()) {
                if (file.isDirectory()) {
                    plugins.addAll(loadPlugins(file, interpreter, manifestName));
                    continue;
                }
                try {
                   T plugin = loadPlugin(file, interpreter, manifestName);
                    if (plugin != null) {
                        plugins.add(loadPlugin(file, interpreter, manifestName));
                    }
                } catch (PluginLoadingException ex) {
                    if (ex.getCause() instanceof ClassNotFoundException) {
                        log(SimplePluginSystemLogger.WARN, "Cannot find specified Main Class.", ex.getCause());
                    } else if (ex.getCause() instanceof ClassCastException) {
                        log(SimplePluginSystemLogger.WARN, "Class does not extend/implement given class/interface.", ex.getCause());
                    } else {
                        log(SimplePluginSystemLogger.ERROR, "Exception encountered while loading plugins.", ex.getCause());
                        throw ex;
                    }
                }
            }
        }
        return plugins;
    }
    
    /**
     * Loads plugins while saving interpreter from specified directory.
     * Manifest will be searched for by file name.
     * 
     * @param directory Directory containing plugin .jar files
     * @param interpreter Interpreter that will be used to interpret manifest
     * @param manifestName First file with that name will count as manifest
     * @return List of plugins and interpreters wrapped in PluginInterpreterPair object
     */
    public List<PluginInterpreterPair<T>> loadPluginsAndInterpreters(File directory, ManifestInterpreter interpreter, String manifestName){
        List<PluginInterpreterPair<T>> plugins = new ArrayList<>();
        if (directory.isDirectory()) {
            for (File file : directory.listFiles()) {
                if (file.isDirectory()) {
                    plugins.addAll(loadPluginsAndInterpreters(file, interpreter, manifestName));
                    continue;
                }
                try {
                    PluginInterpreterPair<T> plugin = loadPluginAndInterpreter(file, interpreter, manifestName);
                    if (plugin != null) {
                        plugins.add(plugin);
                    }
                } catch (PluginLoadingException ex) {
                    if (ex.getCause() instanceof ClassNotFoundException) {
                        log(SimplePluginSystemLogger.WARN, "Cannot find specified Main Class.", ex.getCause());
                    } else if (ex.getCause() instanceof ClassCastException) {
                        log(SimplePluginSystemLogger.WARN, "Class does not extend/implement given class/interface.", ex.getCause());
                    } else {
                        log(SimplePluginSystemLogger.ERROR, "Exception encountered while loading plugins.", ex.getCause());
                        throw ex;
                    }
                }
            }
        }
        return plugins;
    }
    
    /**
     * Loads plugins from specified directory.
     * Manifest will be searched for by file extension.
     * 
     * @param directory Directory containing plugin .jar files
     * @param interpreter Interpreter that will be used to interpret manifest
     * @param manifestExtension First file with that extension will count as manifest
     * @return List of plugin objects
     */
    public List<T> loadPluginsUsingManifestExtension(File directory, ManifestInterpreter interpreter, String manifestExtension) {
        List<T> plugins = new ArrayList<>();
        if (directory.isDirectory()) {
            for (File file : directory.listFiles()) {
                if (file.isDirectory()) {
                    plugins.addAll(loadPluginsUsingManifestExtension(file, interpreter, manifestExtension));
                    continue;
                }
                try {
                    T plugin = loadPluginUsingManifestExtension(file, interpreter, manifestExtension);
                    if (plugin != null) {
                        plugins.add(plugin);
                    }
                } catch (PluginLoadingException ex) {
                    if (ex.getCause() instanceof ClassNotFoundException) {
                        log(SimplePluginSystemLogger.WARN, "Cannot find specified Main Class.", ex.getCause());
                    } else if (ex.getCause() instanceof ClassCastException) {
                        log(SimplePluginSystemLogger.WARN, "Class does not extend/implement given class/interface.", ex.getCause());
                    } else {
                        log(SimplePluginSystemLogger.ERROR, "Exception encountered while loading plugins.", ex.getCause());
                        throw ex;
                    }
                }
            }
        }
        return plugins;
    }
    
    /**
     * Loads plugins while saving interpreter from specified directory.
     * Manifest will be searched for by file extension.
     * 
     * @param directory Directory containing plugin .jar files
     * @param interpreter Interpreter that will be used to interpret manifest
     * @param manifestExtension First file with that extension will count as manifest
     * @return List of plugins and interpreters wrapped in PluginInterpreterPair object
     */
    public List<PluginInterpreterPair<T>> loadPluginsAndInterpretersUsingManifestExtension(File directory, ManifestInterpreter interpreter, String manifestExtension) {
        List<PluginInterpreterPair<T>> plugins = new ArrayList<>();
        if (directory.isDirectory()) {
            for (File file : directory.listFiles()) {
                if (file.isDirectory()) {
                    plugins.addAll(loadPluginsAndInterpretersUsingManifestExtension(file, interpreter, manifestExtension));
                    continue;
                }
                try {
                    PluginInterpreterPair<T> plugin = loadPluginAndInterpreterWithManifestExtension(file, interpreter, manifestExtension);
                    if (plugin != null) {
                        plugins.add(plugin);
                    }
                } catch (PluginLoadingException ex) {
                    if (ex.getCause() instanceof ClassNotFoundException) {
                        log(SimplePluginSystemLogger.WARN, "Cannot find specified Main Class.", ex.getCause());
                    } else if (ex.getCause() instanceof ClassCastException) {
                        log(SimplePluginSystemLogger.WARN, "Class does not extend/implement given class/interface.", ex.getCause());
                    } else {
                        log(SimplePluginSystemLogger.ERROR, "Exception encountered while loading plugins.", ex.getCause());
                        throw ex;
                    }
                }
            }
        }
        return plugins;
    }
    
    /**
     * Creates object of specified class using specified constructor parameters.
     * 
     * @param cls Class to create object from
     * @param params Constructor parameters
     * @return Object of the specified class
     */
    private Object createObjectFromClass(Class cls, List<ConstructorParameter> params) {
        Object object = null;
        Constructor constructor = null;
        try {
            constructor = cls.getConstructor(createTypeArray(params));
        } catch (NoSuchMethodException ex) {
            throw new PluginLoadingException("Cannot find suitable constructor", ex);
        } catch (SecurityException ex) {
            //Should not be happening
            throw new PluginLoadingException("There was a problem while loading a plugin", ex);
        }
        boolean wasAccesible = true;
        if (!constructor.canAccess(null)) {
            wasAccesible = false;
            constructor.setAccessible(true);
        }
        try {
            object = constructor.newInstance(createObjectArray(params));
        } catch (IllegalAccessException | InvocationTargetException ex) {
            //Should not be happening
            throw new PluginLoadingException("There was a problem while loading a plugin", ex);
        } catch (InstantiationException ex) {
            throw new PluginLoadingException("Cannot instantiate plugin object", ex);
        }
        if (!wasAccesible) {
            constructor.setAccessible(false);
        }
        return object;
    }
    
    /**
     * Creates array of types used to find suitable constructor from
     * List of constructor parameters.
     * 
     * @param params Constructor parameters
     * @return Array of types
     */
    private Class[] createTypeArray(List<ConstructorParameter> params) {
        Class[] types = new Class[params.size()];
        for (int i = 0; i < params.size(); i++) {
            types[i] = params.get(i).getType();
        }
        return types;
    }
    
    /**
     * Creates array of objects used to create object from suitable constructor
     * from List of constructor parameters.
     * 
     * @param params Constructor parameters
     * @return Array of objects
     */
    private Object[] createObjectArray(List<ConstructorParameter> params) {
        List<Object> objects = new ArrayList<>();
        params.forEach(param -> {
            objects.add(param.getObject());
        });
        return objects.toArray();
    }
    
    /**
     * Returns class with specified name from specified file. 
     * 
     * @param file .jar file
     * @param name Class name (ex. tech.elbit.simplepluginsystem.PluginLoader)
     * @return Class with specified name from specified file
     * @throws ClassNotFoundException
     * @throws MalformedURLException 
     */
    private Class getClassFromJarFile(File file, String name) throws ClassNotFoundException, MalformedURLException {
        return getClassFromJarFile(file.toURI().toURL(), name);
    }
    
    /**
     * Returns class with specified name from specified file. 
     * 
     * @param url .jar file url
     * @param name Class name (ex. tech.elbit.simplepluginsystem.PluginLoader)
     * @return Class with specified name from specified file
     * @throws ClassNotFoundException
     */
    private Class getClassFromJarFile(URL url, String name) throws ClassNotFoundException {
        ClassLoader cl = new URLClassLoader(new URL[]{url});
        Class output = cl.loadClass(name);
        return output;
    }
    
    /**
     * Uses logger to log specific message with cause.
     * 
     * @param level Logging level.
     * @param message Message.
     * @param cause Cause of logging.
     */
    private void log(int level, String message, Throwable cause) {
        if (loggers.isEmpty()) {
            loggers.add(new DefaultSimplePluginSystemLogger());
            log(SimplePluginSystemLogger.INFO, "Logger unspecified. Defaulting to DefaultSimplePluginSystemLogger.");
            log(level, message, cause);
        }
        loggers.forEach(logger -> {
            logger.log(level, message, this.getClass(), cause);
        });
    }
    
    /**
     * Uses logger to log specific message.
     * 
     * @param level Logging level.
     * @param message Message.
     */
    private void log(int level, String message) {
        log(level, message, null);
    }
    
}
