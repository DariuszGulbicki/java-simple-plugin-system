package tech.elbit.simplepluginsystem.exception;

/**
 * Indicates a problem while interpreting a manifest.
 * Usually thrown when exception is caugth inside ManifestInterpreter (Manifest class).
 * 
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public class ManifestInterpretationException extends RuntimeException{

    /**
     * Constructor specifying Error message and cause.
     * 
     * @param message Error message
     * @param cause Error cause
     */
    public ManifestInterpretationException(String message, Throwable cause) {
        super(message, cause);
    }
    
    /**
     * Constructor specifying Error message.
     * 
     * @param message Error message
     */
    public ManifestInterpretationException(String message) {
        super(message);
    }
    
}
