package tech.elbit.simplepluginsystem.exception;

/**
 * Indicates a problem while loading a plugin.
 * Usually thrown when exception is caugth inside PluginLoader.
 * 
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 * @see PluginLoader
 */
public class PluginLoadingException extends RuntimeException {

    /**
     * Constructor specifying Error message and cause.
     * 
     * @param message Error message
     * @param cause Error cause
     */
    public PluginLoadingException(String message, Throwable cause) {
        super(message, cause);
    }
    
    /**
     * Constructor specifying Error message.
     * 
     * @param message Error message
     */
    public PluginLoadingException(String message) {
        super(message);
    }
    
}
