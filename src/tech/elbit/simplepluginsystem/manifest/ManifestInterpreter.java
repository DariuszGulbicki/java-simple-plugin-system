package tech.elbit.simplepluginsystem.manifest;

/**
 * Abstract class representing manifest interpreter.<br>
 * Manifest interpreter is used to extract information about plugin from manifest file.
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public abstract class ManifestInterpreter {

    protected String manifest;
    
    /**
     * Class constructor specifying manifest to interpret.
     * 
     * @param manifest Manifest to interpret represented as String.
     */
    public ManifestInterpreter(String manifest) {
        this.manifest = manifest;
    }
    
    /**
     * Default class constructor.
     */
    public ManifestInterpreter() {
    }
    
    /**
     * Sets and automatically interprets manifest.
     * 
     * @param manifest Manifest to interpret.
     */
    public void setManifest(String manifest) {
        this.manifest = manifest;
    }
    
    /**
     * Returns manifest in String form.
     * 
     * @return Manifest as String.
     */
    public String getManifest() {
        return manifest;
    }
    
    /**
     * Returns path to main plugin class.
     * 
     * @return Main class path.
     */
    public abstract String getPluginClass();
    
}
