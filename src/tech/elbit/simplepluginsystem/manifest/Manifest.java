package tech.elbit.simplepluginsystem.manifest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import tech.elbit.simplepluginsystem.exception.ManifestInterpretationException;

/**
 * Class containing methods used to extract manifest from specified .jar file.
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public class Manifest {
    
    /**
     * Extract manifest from specified .jar file.<br>
     * Manifest will be searched for by file name.
     * 
     * @param file Jar file.
     * @param name Manifest name.
     * @return Manifest as String.
     */
    public static String getManifestForFile(File file, String name) {
        try (ZipInputStream zip = new ZipInputStream(new FileInputStream(file))) {
            ZipEntry entry = zip.getNextEntry();
            while (entry != null) {
                if (entry.getName().equals(name) && !entry.isDirectory()) {
                    String output = new String(zip.readAllBytes());
                    zip.close();
                    return output;
                } else {
                    entry = zip.getNextEntry();
                }
            }
        } catch (FileNotFoundException ex) {
            throw new ManifestInterpretationException("Cannot find .jar file", ex);
        } catch (IOException ex) {
            throw new ManifestInterpretationException("There was an error while interpreing a manifest", ex);
        }
        return null;
    }
    
    /**
     * Extract manifest from specified .jar file.<br>
     * Manifest will be searched for by file extension.
     * 
     * @param file Jar file.
     * @param extension Manifest file extension.
     * @return Manifest as String.
     */
    public static String getManifestForFileExtension(File file, String extension) {
        try (ZipInputStream zip = new ZipInputStream(new FileInputStream(file))) {
            ZipEntry entry = zip.getNextEntry();
            while (entry != null) {
                if (entry.getName().endsWith(extension) && !entry.isDirectory()) {
                    String output = new String(zip.readAllBytes());
                    zip.close();
                    return output;
                } else {
                    entry = zip.getNextEntry();
                }
            }
        } catch (FileNotFoundException ex) {
            throw new ManifestInterpretationException("Cannot find .jar file", ex);
        } catch (IOException ex) {
            throw new ManifestInterpretationException("There was an error while interpreing a manifest", ex);
        }
        return null;
    }
    
}
