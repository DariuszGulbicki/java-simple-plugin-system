package tech.elbit.simplepluginsystem.manifest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Default interpreter used to interpret manifest file.<br>
 * Manifest must be written in <strong>key value pairs</strong>.<br>
 * <em>ex. testKey1=testValue1; testKey2 = testValue2;</em><br>
 * All spaces inside manifest will be deleted so test Key 1 = t e s t will become testKey1=test.<br>
 * To include special characters (space, equal sign, semicolon) text must be wrapped in "" symbols, 
 * so <em>"e x a m p l e K e y" = "e x a m p l e V a l u e" will become e x a m p l e K e y=e x a m p l e V a l u e.</em><br>
 * Manifest can also contain single attributes whitch may be wrapped in "".<br>
 * <em>ex. attribute1; attribute2; "attribute 3";</em>
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public class DefaultManifestInterpreter extends ManifestInterpreter{
    
    private String pluginClassAttributeName;
    
    private Map<String, String> attributePairs = new HashMap<>();
    private List<String> attributes = new ArrayList<>();
    
    @Deprecated(since="0.1")
    public DefaultManifestInterpreter(String manifest, String pluginClassAttributeName) {
        super(manifest);
        if (pluginClassAttributeName == null) throw new IllegalArgumentException("pluginClassAttributeName cannot be null");
        this.pluginClassAttributeName = pluginClassAttributeName;
        interpretManifest(manifest);
    } 
    
    /**
     * Constructor specifying name of the attribute pair containing path to plugin class.
     * 
     * @param pluginClassAttributeName Name of the attribute pair containing path to plugin class .class file (package.className)
     */
    public DefaultManifestInterpreter(String pluginClassAttributeName) {
        if (pluginClassAttributeName == null) throw new IllegalArgumentException("pluginClassAttributeName cannot be null");
        this.pluginClassAttributeName = pluginClassAttributeName;
    }

    /**
     * Sets manifest to interpret
     * 
     * @param manifest Manifest to interpret in String form
     */
    @Override
    public void setManifest(String manifest) {
        this.manifest = manifest;
        interpretManifest(manifest);
    }
    
    /**
     * Returns attribute pairs (key=value;) as map
     * 
     * @return Attribute pairs as map
     */
    public Map<String, String> getAttributePairs() {
        return attributePairs;
    }

    /**
     * Returns single attributes (attribute;) as list
     * 
     * @return Attributes as list
     */
    public List<String> getAttributes() {
        return attributes;
    }
    
    /**
     * Plugin class location derived from manifest.<br>
     * Location is derived from attribute pair with key specified in constructor.
     * 
     * @return Plugin class location derived from manifest
     */
    @Override
    public String getPluginClass() {
        return attributePairs.get(pluginClassAttributeName);
    }
    
    /**
     * Interprets manifest
     * 
     * @param manifest Manifest in string form
     */
    private void interpretManifest(String manifest) {
        for (String manifestAttributes : prepareText(manifest).split(";")) {
            if (manifestAttributes.isEmpty()) continue;
            String[] attribute = manifestAttributes.split("=");
            if (attribute.length == 1) {
                this.attributes.add(attribute[0]);
            } else if (attribute.length == 2) {
                this.attributePairs.put(attribute[0], attribute[1]);
            }
        }
    }
    
    /**
     * Removes spaces and endlines that are not wrapped in "" or ''
     * 
     * @param text Text to preapre
     * @return Prepared text
     */
    private String prepareText(String text) {
        String output = "";
        boolean isText = false;
        for (int i = 0; i < text.length(); i++) {
            char currentChar = text.charAt(i);
            switch (currentChar) {
                case '"':
                case '\'':
                    isText = !isText;
                    break;
                case '\n':
                    if (isText) {
                        output += currentChar;
                    }   break;
                case ' ':
                    if (isText) {
                        output += currentChar;
                    }   break;
                default:
                    output += currentChar;
                    break;
            }
        }
        return output;
    }

}
