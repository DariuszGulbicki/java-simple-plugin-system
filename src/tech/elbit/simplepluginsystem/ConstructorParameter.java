package tech.elbit.simplepluginsystem;

/**
 * Contains type (Class) and object used to find constructor and create object in PluginLoader.
 * 
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 * @see PluginLoader
 */
public class ConstructorParameter {

    private Class type;
    private Object object;
    
    /**
     * Constructor accepting Class type and object of that class.
     * 
     * @param type Class used to find suitable constructor
     * @param object Object of Class type used to create instance from found constructor
     */
    public ConstructorParameter(Class type, Object object) {
        this.type = type;
        this.object = object;
    }

    /**
     * Returns Class type.
     * 
     * @return Class type
     */
    public Class getType() {
        return type;
    }

    /**
     * Sets Class type.
     * 
     * @param type Class type
     */
    public void setType(Class type) {
        this.type = type;
    }

    /**
     * Returns object.
     * 
     * @return object
     */
    public Object getObject() {
        return object;
    }

    /**
     * Sets object.
     * 
     * @param object Object to set
     */
    public void setObject(Object object) {
        this.object = object;
    }
    
}
