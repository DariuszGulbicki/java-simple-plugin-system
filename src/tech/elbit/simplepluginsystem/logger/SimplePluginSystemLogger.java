package tech.elbit.simplepluginsystem.logger;

/**
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public abstract class SimplePluginSystemLogger {

    public static final int FINE = -1;
    public static final int INFO = 0;
    public static final int WARN = 1;
    public static final int ERROR = 2;
    public static final int FATAL = 3;
    
    /**
     * Logs message on text level.
     * 
     * @param message Log message.
     */
    public abstract void text(String message);
    
    /**
     * Logs message on fine level.
     * 
     * @param message Log message.
     * @param origin Log origin.
     * @param cause Log cause.
     */
    public abstract void fine(String message, Class origin, Throwable cause);
    
    /**
     * Logs message on info level.
     * 
     * @param message Log message.
     * @param origin Log origin.
     * @param cause Log cause.
     */
    public abstract void info(String message, Class origin, Throwable cause);
    
    /**
     * Logs message on warn level.
     * 
     * @param message Log message.
     * @param origin Log origin.
     * @param cause Log cause.
     */
    public abstract void warn(String message, Class origin, Throwable cause);
    
    /**
     * Logs message on error level.
     * 
     * @param message Log message.
     * @param origin Log origin.
     * @param cause Log cause.
     */
    public abstract void error(String message, Class origin, Throwable cause);
    
    /**
     * Logs message on fatal level.
     * 
     * @param message Log message.
     * @param origin Log origin.
     * @param cause Log cause.
     */
    public abstract void fatal(String message, Class origin, Throwable cause);
    
    /**
     * Logs message on fine level.
     * 
     * @param message Log message.
     * @param origin Log origin.
     */
    public void fine(String message, Class origin) {
        fine(message, origin, null);
    }
    
    /**
     * Logs message on info level.
     * 
     * @param message Log message.
     * @param origin Log origin.
     */
    public void info(String message, Class origin) {
        info(message, origin, null);
    }
    
    /**
     * Logs message on warn level.
     * 
     * @param message Log message.
     * @param origin Log origin.
     */
    public void warn(String message, Class origin) {
        warn(message, origin, null);
    }
    
    /**
     * Logs message on error level.
     * 
     * @param message Log message.
     * @param origin Log origin.
     */
    public void error(String message, Class origin) {
        error(message, origin, null);
    }
    
    /**
     * Logs message on fatal level.
     * 
     * @param message Log message.
     * @param origin Log origin.
     */
    public void fatal(String message, Class origin) {
        fatal(message, origin, null);
    }
    
    /**
     * Logs message on fine level.
     * 
     * @param message Log message.
     */
    public void fine(String message) {
        fine(message, null, null);
    }
    
    /**
     * Logs message on info level.
     * 
     * @param message Log message.
     */
    public void info(String message) {
        info(message, null, null);
    }
    
    /**
     * Logs message on warn level.
     * 
     * @param message Log message.
     */
    public void warn(String message) {
        warn(message, null, null);
    }
    
    /**
     * Logs message on error level.
     * 
     * @param message Log message.
     */
    public void error(String message) {
        error(message, null, null);
    }
    
    /**
     * Logs message on fatal level.
     * 
     * @param message Log message.
     */
    public void fatal(String message) {
        fatal(message, null, null);
    }
    
    /**
     * Logs message on selected level.
     * 
     * @param level Log level.
     * @param message Log message.
     * @param origin Log origin.
     * @param cause Log cause.
     */
    public void log(int level, String message, Class origin, Throwable cause) {
        switch (level) {
            case -1:
                fine(message, origin, cause);
                break;
            case 0:
                info(message, origin, cause);
                break;
            case 1:
                warn(message, origin, cause);
                break;
            case 2:
                error(message, origin, cause);
                break;
            case 3:
                fatal(message, origin, cause);
                break;
            default:
                text(message);
        }
    }
    
    /**
     * Logs message on selected level.
     * 
     * @param level Log level.
     * @param message Log message.
     * @param origin Log origin.
     */
    public void log(int level, String message, Class origin) {
        log(level, message, origin, null);
    }
    
    /**
     * Logs message on selected level.
     * 
     * @param level Log level.
     * @param message Log message.
     */
    public void log(int level, String message) {
        log(level, null, null);
    }
    
}
