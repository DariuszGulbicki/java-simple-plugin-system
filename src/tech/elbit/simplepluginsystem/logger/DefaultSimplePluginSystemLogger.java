package tech.elbit.simplepluginsystem.logger;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public class DefaultSimplePluginSystemLogger extends SimplePluginSystemLogger{

    private boolean displayFractions = false;
    private boolean displayTimezone = false;
    
    private int loggingLevel = SimplePluginSystemLogger.WARN;
    
    /**
     * Class constructor specifying displayfractions and displayTimezone.<br>
     * If displayFractions is true logger will print fractions of a second.<br>
     * If displayTimezone is true logger will print current timezone.
     * 
     * @param displayFractions If true logger will print fractions of a second.
     * @param displayTimezone If true logger will print current timezone.
     */
    public DefaultSimplePluginSystemLogger(boolean displayFractions, boolean displayTimezone) {
        this.displayFractions = displayFractions;
        this.displayTimezone = displayTimezone;
    }
    
    /**
     * Class constructor specifying displayfractions and displayTimezone.<br>
     * If displayFractions is true logger will print fractions of a second.
     * 
     * @param displayFractions If true logger will print fractions of a second.
     */
    public DefaultSimplePluginSystemLogger(boolean displayFractions) {
        this.displayFractions = displayFractions;
    }
    
    /**
     * Default class constructor.
     */
    public DefaultSimplePluginSystemLogger() {}
    
    @Override
    public void text(String message) {
        System.out.println(getDateTimePrefix() + message);
    }

    @Override
    public void fine(String message, Class origin, Throwable cause) {
        if (loggingLevel <= SimplePluginSystemLogger.FINE)
        System.out.println(getDateTimePrefix() + " \u001B[42mFINE\u001B[0m " + prepareOriginString(origin) + " " + message + " " + prepareCauseString(cause));
    }

    @Override
    public void info(String message, Class origin, Throwable cause) {
        if (loggingLevel <= SimplePluginSystemLogger.INFO)
        System.out.println(getDateTimePrefix() + " \u001B[44mINFO\u001B[0m " + prepareOriginString(origin) + " " + message + " " + prepareCauseString(cause));
    }

    @Override
    public void warn(String message, Class origin, Throwable cause) {
        if (loggingLevel <= SimplePluginSystemLogger.WARN)
        System.out.println(getDateTimePrefix() + " \u001B[43mWARN\u001B[0m " + prepareOriginString(origin) + " " + message + " " + prepareCauseString(cause));
    }

    @Override
    public void error(String message, Class origin, Throwable cause) {
        if (loggingLevel <= SimplePluginSystemLogger.ERROR)
        System.out.println(getDateTimePrefix() + " \u001B[41mERROR\u001B[0m " + prepareOriginString(origin) + " " + message + " " + prepareCauseString(cause));
    }

    @Override
    public void fatal(String message, Class origin, Throwable cause) {
        if (loggingLevel <= SimplePluginSystemLogger.FATAL)
        System.out.println(getDateTimePrefix() + " \u001B[41m! FATAL !\u001B[0m " + prepareOriginString(origin) + " \u001B[31m" + message + "\u001B[0m " + prepareCauseString(cause));
    }
    
    /**
     * Sets level from wich the messages will be logged.<br>
     * Equivalent to SimplePluginSystemLogger.WARN by default.
     * 
     * @param loggingLevel Level from wich the messages will be logged. (example: SimplePluginSystemLogger.ERROR)
     */
    public void setLoggingLevel(int loggingLevel) {
        this.loggingLevel = loggingLevel;
    }
    
    /**
     * returns level from wich the messages will be logged.<br>
     * Equivalent to SimplePluginSystemLogger.WARN by default.
     * 
     * @return Logging level.
     */
    public int getLoggingLevel() {
        return loggingLevel;
    }

    /**
     * Returns formatted date and time prefix.<br>
     * If displayFractions is true will add fractions of a second.<br>
     * If displayTimezone is true will add current timezone.
     * @return Formatted date and time prefix.
     */
    private String getDateTimePrefix() {
        OffsetDateTime now = OffsetDateTime.now();
        String output = "";
        if (displayTimezone) {
            String timeZone = DateTimeFormatter.ofPattern("OOOO").format(now);
            output += "\033[0;90m(" + timeZone + ")\033[0m ";
        }
        String dateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(now);
        output += "[" + dateTime;
        if (displayFractions) {
            String fractions = DateTimeFormatter.ofPattern("SSS").format(now);
            output += "\033[0;90m." + fractions + "]\033[0m ";
        } else {
            output += "] ";
        }
        return output;
    }
    
    /**
     * Returns formatted origin class name.
     * 
     * @param origin Class from wich the message is logged.
     * @return 
     */
    private String prepareOriginString(Class origin) {
        if (origin == null) {
            return "";
        } else {
            return " " + origin.getSimpleName() +" >>";
        }
    }
    
    /**
     * Returns formatted cause nameand localized message.
     * 
     * @param cause Log cause.
     * @return Formatted cause nameand localized message.
     */
    private String prepareCauseString(Throwable cause) {
        if (cause == null) {
            return "";
        } else {
            return "\u001B[31m(caused by " + cause.getClass().getName() + ": " + cause.getLocalizedMessage() + ")\u001B[0m";
        }
    }
    
}
