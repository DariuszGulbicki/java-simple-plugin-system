package tech.elbit.simplepluginsystem;

import tech.elbit.simplepluginsystem.manifest.ManifestInterpreter;

/**
 * Contains pair of plugin (of class T) and manifest interpreter used to interpret plugin`s manifest.
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @param <T> Plugin class.
 * @since 0.1
 */
public class PluginInterpreterPair<T> {

    private T plugin;
    private ManifestInterpreter interpreter;
    
    /**
     * Default class constructor.
     */
    public PluginInterpreterPair() {
        
    }
    
    /**
     * Class constructor specifying plugin and manifest interpreter to be stored.
     * 
     * @param plugin Plugin to store.
     * @param interpreter Manifest interpreter to store.
     */
    public PluginInterpreterPair(T plugin, ManifestInterpreter interpreter) {
        this.plugin = plugin;
        this.interpreter = interpreter;
    }
    
    /**
     * Returns plugin.
     * 
     * @return Plugin. 
     */
    public T getPlugin() {
        return plugin;
    }

    /**
     * Sets plugin.
     * 
     * @param plugin Plugin. 
     */
    public void setPlugin(T plugin) {
        this.plugin = plugin;
    }

    /**
     * Returns interpreter used to interpret plugin`s manifest.
     * 
     * @return Plugin.
     */
    public ManifestInterpreter getInterpreter() {
        return interpreter;
    }
    
    /**
     * Sets manifest interpreter.
     * 
     * @param interpreter manifest interpreter.
     */
    public void setInterpreter(ManifestInterpreter interpreter) {
        this.interpreter = interpreter;
    }
    
}
