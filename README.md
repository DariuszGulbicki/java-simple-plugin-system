[![pipeline status](https://gitlab.com/DariuszGulbicki/java-simple-plugin-system/badges/master/pipeline.svg)](https://gitlab.com/DariuszGulbicki/java-simple-plugin-system/-/commits/main)
[![coverage report](https://gitlab.com/DariuszGulbicki/java-simple-plugin-system/badges/master/coverage.svg)](https://gitlab.com/DariuszGulbicki/java-simple-plugin-system/-/commits/main)
[![wiki](https://gitlab.com/DariuszGulbicki/java-simple-plugin-system/-/raw/master/gitlab-badges/png/wiki.png?inline=false)](https://gitlab.com/DariuszGulbicki/java-simple-plugin-system/-/wikis/home)
[![download](https://gitlab.com/DariuszGulbicki/java-simple-plugin-system/-/raw/master/gitlab-badges/png/download.png?inline=false)](https://gitlab.com/DariuszGulbicki/java-simple-plugin-system/-/raw/master/jars/SimplePluginSystem-0.1.jar)
# ![Project logo](https://gitlab.com/DariuszGulbicki/java-simple-plugin-system/-/raw/master/logo/logo_scaled_minimized.png?inline=false) Simple Plugin System (Java)

Simple Plugin System is a library to quickly implement plugin integration into your java project. Simple Plugin System aims to deliver minecraft-like plugin installation expirience to the end user, while at the same time providing easy to use interface for the developer.

## Installation

For the time being we don`t have maven or gradle repository. You can download the latest version [here](https://gitlab.com/api/v4/projects/28922215/jobs/artifacts/master/download?job=build).

## Usage

### Setting the scene

Firstly you need a class or interface representing your plugin:

```java
//Plugin interface with two methods (Name can be changed)
public interface Plugin {

    public void start();
    
    public void end();
    
}
```

Then in a plugin create class that implements/extends chosen interface/class and jar with Plugin interface as a library:

```java
public class PluginExample implements Plugin {
    
    @Override
    public void start() {
        System.out.println("Hello, plugin!");
    }
    
    @Override
    public void end() {
        System.out.println("Goodbye, plugin");
    }

}
```

### Loading plugin from .jar file: 

Lets start by creating PluginLoader instance and loading our plugin:

```java
// Initializes plugin loader that creates Plugin objects using constructor without parameters.
PluginLoader<Plugin> loader = new PluginLoader();

/*
* Loads plugin from selected .jar file with specified class (replace class.Name with path to class extending/implementing selected class/interface)
* For example: tech.elbit.pluginexample.PluginExample
*/
Plugin plugin = loader.loadPlugin(new File("path/to/your/file.jar"), "class.Name");
```
Now you can use methods from your plugin class or interface. Congratulations!

```java
plugin.start();
plugin.end();
```
When you run your project you should get something like this:

```
Hello, plugin!
Goodbye, plugin
```

**If you are looking for more examples and documentation check out the [WIKI](https://gitlab.com/DariuszGulbicki/java-simple-plugin-system/-/wikis/Home)**

## Building from source

To build project from source you will need:
* JDK 11 (For example coretto 11)
* Apache ANT

Start by using "cd" command (or equivalent) to navigate into the directory containing project files. Then build the project using ant as in example shown below:

```bash
ant -buildfile build.xml -Dnb.internal.action.name=rebuild clean jar
```

After building the project you can add it as jar library to your project. For more information refer to the [WIKI](https://gitlab.com/DariuszGulbicki/java-simple-plugin-system/-/wikis/Home).

> ### Note
> When Simple Plugin System is compatible with Java 8, it **MUST** be built using JDK 11. Building using any older version of JDK **WILL** cause errors.

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)

> ### Note
>The images in this repository are **NOT** distributed under the [MIT](https://choosealicense.com/licenses/mit/) license. To use the images (project logo for example) you **MUST** obtain a license from Project author or Image author. 
